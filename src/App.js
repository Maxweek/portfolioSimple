import { useEffect } from 'react';
import { _SLIDES } from './components/DATA';
import List from './components/list/list';
import Profile from './components/profile/profile';
import "./css/styles.scss";

function App() {
  return (
    <main className="App mainContent">
      <div className='mainTitle__box'>
        <div className='mainTitle'>
          Портфолио
        </div>
        <Profile />
      </div>
      {_SLIDES.length &&
        <List slides={_SLIDES} />
      }
      <div className='copyright'>© Неделько М.И., 2023</div>
    </main>
  );
}

export default App;

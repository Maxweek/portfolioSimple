import { useState } from "react";
import Image from "../image/image";
import Slider from "../slider/slider";
import "./styles.scss";

export default function Slide(props) {
    const [ready, setReady] = useState(false)

    let imageAcc = 0;

    function onLoad() {
        imageAcc++;
        if (imageAcc === props.images.length) {
            setReady(true)
        }
    }

    return (
        <div className={"slide" + (ready ? " __ready" : '')}>
            <div className="blury" style={{ backgroundColor: props.color }}></div>
            <div className="inner">
                <SlideTop {...props} />
                <div className="row">
                    <div className="col">
                        <SlideImages {...props} ready={ready} onLoad={onLoad} />
                    </div>
                </div>
                <div className="row">
                    <div className="col content">
                        <div className="urls">
                            {props.urls.map(url => <a href={url} target="_blank">{url}</a>)}
                        </div>
                        <div className="description">
                            {props.description}
                        </div>
                    </div>
                </div>
                <div className="row bot">
                    <div className="col">
                        <SlideBox title={"Особенности"} list={props.additional} />
                    </div>
                    <div className="col">
                        <SlideBox title={"Стек"} list={props.stack} />
                    </div>
                </div>
            </div>
        </div>
    )
}

function SlideTop(props) {
    return (
        <div className="top">
            <div className="title">
                {props.title}
            </div>
            <div className="year">
                {props.year}
            </div>
            <div className={"available" + (props.available ? ' __opened' : " __closed")} >
                {props.available ? 'Есть доступ' : 'Нет доступа'}
            </div>
            <div className="divider"></div>
            <div className={"logo" + (props.darkLogo ? ' __dark' : "")} >
                {props.logo.map(img => <img src={'/assets/' + props.dir + '/' + img} />)}
            </div>
        </div>
    )
}
function SlideImages(props) {
    return (
        <div className="images">
            <Slider ready={props.ready} slides={props.images.map(img => <Image src={'/assets/' + props.dir + '/' + img} onLoad={props.onLoad} />)} />
        </div>
    )
}
function SlideBox(props) {
    return (
        <div className="box">
            <div className="box_title">
                {props.title}
            </div>
            <ul className="box_body">
                {props.list.map(item => <li>{item}</li>)}
            </ul>
        </div>
    )
}
import './styles.scss'

export default function Profile() {

    return (
        <div className='profile'>
            <div className='profile__image'>
                <img src={'/profile_image.jpg'} />
            </div>
            <div className='profile__name'>Максим<br />Неделько</div>
        </div>
    )
}
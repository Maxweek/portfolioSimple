import { Swiper, SwiperSlide } from "swiper/react";
import { useEffect, useRef, useState } from "react";
import "./styles.scss";
import "swiper/css";

export default function Slider(props) {
    const [mainSwiper, setMainSwiper] = useState(null);
    const [ready, setReady] = useState(false);

    useEffect(() => {
        if (props.ready && !mainSwiper.destroyed && mainSwiper) {
            //
        }
        if (typeof props.ready === 'undefined') {
            setReady(true)
        }
    }, [props.ready, mainSwiper, ready])

    useEffect(() => {
        if (ready) {
            mainSwiper.slideTo(0)
        }
    }, [ready])

    function onUpdate(){
        setReady(false)
        setTimeout(() => {
            setReady(true)
        }, 500)
    }

    return (
        <div className={"slider" + (ready ? " __ready" : '')}>
            <Swiper
                spaceBetween={20}
                slidesPerView={'auto'}
                centeredSlides={true}
                slideToClickedSlide={true}
                followFinger={true}
                onInit={onUpdate}
                onUpdate={onUpdate}
                onSwiper={swiper => {
                    setMainSwiper(swiper)
                }}
            >
                {props.slides.map(el => <SwiperSlide>{el}</SwiperSlide>)}
            </Swiper>
        </div>
    )
}
import { useEffect, useRef, useState } from "react";

export default function Image(props) {
    const image = useRef();
    const [type, setType] = useState('image')
    const [orientation, setOrientation] = useState('')
    const [loaded, setLoaded] = useState(false)

    useEffect(() => {
        if(props.src.split('.')[1] === 'mp4'){
            setType('video')
        } else {
            setType('image')
        }
    }, [])

    useEffect(() => {
        if(loaded){
            if(image.current.clientWidth > image.current.clientHeight){
                setOrientation('h')
            } else {
                setOrientation('v')
            }
        }
    }, [loaded, image])

    function onLoad(){
        setLoaded(true)
        if(typeof props.onLoad === 'function'){
            props.onLoad()
        }
    }

    return (
        <div className={"imageBox" + " __" + type + " __" + orientation}>
            {type === 'image' && <img src={props.src} ref={image} onLoad={onLoad} />}
            {type === 'video' && <video src={props.src} ref={image} onLoadedMetadata={onLoad} loop="loop" muted="muted" autoPlay="autoplay" type="video/mpeg" preload="auto" playsInline="" />}
        </div>
    )
}
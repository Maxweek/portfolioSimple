import Slide from "../slide/slide";
import "./styles.scss";

export default function List(props) {
    function getSlides() {
        return props.slides.map(slide => {
            return <Slide {...slide} />
        })
    }
    return (
        <div className="list">
            {getSlides()}
        </div>
    )
}